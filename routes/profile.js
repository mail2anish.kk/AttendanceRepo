var express = require('express');
var router = express.Router();
var multer = require('multer');

var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

var dbUtil = require("../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;


var storage = multer.diskStorage({
    destination: function (req, file, cb) {


        cb(null, 'upload/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname)
    }
});

var upload = multer({storage: storage}).single('file');

/*
 router.post('/', function (req, res) {

 upload(req, res, function (err) {
 if (err) {
 // An error occurred when uploading

 console.log(JSON.stringify(err));

 }
 console.log('no error');
 res.json({
 success:true,
 message:'File Uploaded!'
 });
 })
 });
 */
router.post('/', function (req, res) {


    var exceltojson; //Initialization

    upload(req, res, function (err) {
        if (err) {
            res.json({error_code: 1, err_desc: err});
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            res.json({error_code: 1, err_desc: "No file passed"});
            return;
        }
        //start convert process
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {

            exceltojson({
                input: req.file.path, //the same path where we uploaded our file
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result) {
                if (err) {
                    return res.json({error_code: 1, err_desc: err, data: null});
                }

                try {
                    var yearId = req.body.yearId;
                    var branch = req.body.branch;
                    var subject = req.body.subject;

                    if (!branch || !subject || !yearId) {
                        return res.redirect('kryptosattendance/dashboard#/home');
                    }

                    for (var i = 0; i < result.length; i++) {
                        result[i]['year'] = yearId;
                        result[i]['branch'] = branch;
                        result[i]['subject'] = subject;

                    }
                    dbUtil.getConnection(function (db) {
                        var tenant = req.user.tenant;
                        var tableName = "T_" + tenant + "_" + yearId + "_" + branch + "_" + subject ;
                        db.collection(tableName).drop();
                        db.collection(tableName).insert(
                            result
                            , function (err, result3) {
                                res.json({
                                    status: true,
                                    message: 'Sheet added successfully'
                                });
                            });
                    });

                } catch (e) {
                    res.json({
                        "error": "Some error occurred. Please try again."
                    });
                }

            });
        } catch (e) {
            res.json({error_code: 1, err_desc: "Corrupted excel file"});
        }
    });
});


module.exports = router;