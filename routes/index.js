var express = require('express');
var router = express.Router();


/*Local import*/
var authenticator = require('../authenticate/authenticator.js');

/* GET home page.
 router.get('/', function(req, res, next) {
 console.log("Index called..!!");
 res.render('index', { title: 'Express' });
 });*/


/* GET home page. */
router.get('http://localhost:3000/kryptosattendance/dashboard', authenticator.ensureAuthenticated, function (req, res) {
    res.render('/login');
});

module.exports = router;
