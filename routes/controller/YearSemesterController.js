"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.addYear = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.year) {

        var tenant = req.body.tenant
        var year = req.body.year
        var yearId = req.body.yearId
        try {
            var data = {
                'tenant': tenant,
                'year': year,
                'yearId': yearId
                 };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Year";
                db.collection(tableName).find({
                    "yearId": yearId
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({
                            "error": "YearId already added"
                        });
                    }else{
                        db.collection(tableName).insertOne(data, function(err, result3) {
                            if (!err) {
                                res.json({
                                    "success": "Year added"
                                });
                            }
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getYear = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Year";
                db.collection(tableName).find({
                }).sort( { yearId: 1 } ).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({'year':result});
                    }else{
                        res.json({
                            "error": "No Year added"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}