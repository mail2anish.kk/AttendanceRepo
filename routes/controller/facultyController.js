"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.addFaculty = function(req, res, next) {
    if (req.body && req.body.facultyName && req.body.tenant) {

        var facultyName = req.body.facultyName
        var tenant = req.body.tenant
        try {
            var data = {
                'facultyName': facultyName
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Faculties";
                db.collection(tableName).find({
                    "facultyName": facultyName
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({
                                "error": "Faculty already added"
                            });
                    }else{
                    db.collection(tableName).insertOne(data, function(err, result3) {
                        if (!err) {
                            res.json({
                                "success": "Faculty added"
                            });
                        }
                    });
                }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getFaculty = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Faculties";
                db.collection(tableName).find({
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({'Faculties':result});
                    }else{
                            res.json({
                                "error": "No faculty added"
                            });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}