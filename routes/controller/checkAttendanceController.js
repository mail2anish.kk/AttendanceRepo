"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.getStudentsAttendance = function(req, res, next) {
    if (req.body && req.body.qrCode && req.body.stTime && req.body.endTime && req.body.branchName && req.body.branchId && req.body.subject && req.body.subjectId && req.body.year && req.body.facultyName && req.body.tenant) {

        var qrCode = req.body.qrCode
        var branchName = req.body.branchName
        var branchId = req.body.branchId
        var year = req.body.year
        var subject = req.body.subject
        var subjectId = req.body.subjectId
        var facultyName = req.body.facultyName
        var tenant = req.body.tenant
        var endTime = req.body.endTime
        var stTime = req.body.stTime
        try {
            var dateTimeStamp = new Date();
            var data = {
                'qrCode': qrCode,
                'branchName': branchName,
                'branchId':branchId,
                'year': year,
                'subject': subject,
                'subjectId': subjectId,
                'facultyName': facultyName,
                'stTime': stTime,
                'endTime': endTime,
                'activatedtime': dateTimeStamp.toString()
            };
            dbUtil.getConnection(function(db) {
                    var tableName = "T_" + tenant + "_activeQrCodes";
                    db.collection(tableName).find({
                        "qrCode": qrCode
                    }).toArray(function(err, result) {
                        if (result.length > 0) {
                            res.json({
                                "activeClass": result
                            });
                        } else {
                            db.collection(tableName).insertOne(data, function(err, result3) {
                                res.json({
                                    "success": "class activated"
                                });
                            });
                        }
                    });
            });
    } catch (e) {
        res.json({
            "error": "Some error occurred. Please try again."
        });
    }
} else {
    res.status(401).json({
        "error": "Parameters missing"
    });
}
}


