var express = require('express');

var router = express.Router();

var dbUtil = require('../config/dbUtil.js');
var ObjectId = require('mongodb').ObjectID;

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var authenticator = require('./../authenticate/authenticator.js');

router.get('/', authenticator.ensureAuthenticated, function (req, res, next) {
    console.log("CAlled kryptosattendance/dashboard")
    res.render('index', {title: 'Express'});
});

/*router.get('/login', function(req, res, next) {
 res.render('login', { title: 'Express' });
 });*/

router.get('/login', function (req, res, next) {
    console.log("CAlled kryptosattendance/dashboard")
    res.render('login', {title: 'Express'});
});

router.get('/partials/:name', function (req, res, next) {
    var name = req.params.name;
    res.render('partials/' + name);
});


router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/kryptosattendance/dashboard',
        failureRedirect: '/kryptosattendance/dashboard/login',
        failureFlash: true
    }),
    function (req, res) {
        res.redirect('/');
    });

passport.use(new LocalStrategy(
    function (email, password, done) {
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + "UserRecords";
            db.collection(tableName).find({email: email}).toArray(function (err, result) {
                if (result.length === 0) {
                    console.log('user doesnt  exist');
                    return done(null, false, {message: 'User not exist.'});
                }
                else {
                    var user = result[0];

                    if (user['password'] !== password) {
                        console.log('password didnt matched');
                        return done(null, false, {message: 'Password does not match.'});
                    }
                    else {
                        console.log('password matched');
                        return done(null, user);
                    }
                }
            })
        });

    }));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});


module.exports = router;